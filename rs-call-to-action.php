<?php
/**
 * Plugin Name:     Rs Call To Action
 * Plugin URI:      https://ronniestevens.co
 * Description:     Add Call to Action to Featured Image as a banner
 * Author:          Ronnie Stevens
 * Author URI:      https://ronniestevens.co
 * Text Domain:     rs-call-to-action
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Rs_Call_To_Action
 */

// Your code starts here.
function add_rs_scripts() {
    wp_enqueue_style( 'rs-call-to-action-css', plugins_url('rs-call-to-action/css/rs-call-to-action.css'), array(), '1.0', 'all');
    wp_enqueue_script( 'rs-call-to-action-script', plugins_url('rs-call-to-action/js/rs-call-to-action.js'), array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'add_rs_scripts' );

function add_rs_admin_scripts() {
    wp_enqueue_style( 'css', plugins_url('rs-call-to-action/css/rs-admin.css'), array(), '1.0', 'all');
}
add_action('admin_enqueue_scripts', 'add_rs_admin_scripts' );

if ( ! function_exists ( 'setup_banner' ) ) {
    function setup_banner() {
        if( has_post_thumbnail() ) {
            $rs_call_to_action_options = get_option( 'rs_call_to_action_option_name' ); // Array of All Options
            $height_in_pixels_0 = $rs_call_to_action_options['height_in_pixels_0']; // Height in pixels
            $banner_height = get_post_meta(get_the_ID(), 'meta-height', true);
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
            $banner_bgsize = get_post_meta(get_the_ID(), 'meta-select-bgsize', true);
            $banner_bgpos = get_post_meta(get_the_ID(), 'meta-select-bgpos', true);
            $banner_bgrepeat = get_post_meta(get_the_ID(), 'meta-select-bgrepeat', true);
            $banner_text = nl2br( esc_html(get_post_meta(get_the_ID(),'meta-textarea', true) ) );
            $banner_textcolor = get_post_meta(get_the_ID(), 'meta-text-color', true);
            $banner_button = get_post_meta(get_the_ID(), 'meta-text', true);
            $banner_color = get_post_meta(get_the_ID(), 'meta-color', true);
            $banner_linkpage = get_post_meta(get_the_ID(), 'meta-select', true);
            $banner_image_url = get_post_meta(get_the_ID(), 'meta-image', true);
            $banner_local_link = '#'.get_post_meta(get_the_ID(), 'meta-local-link', true);
            $style = '<style>';
            $style .= '.banner {';
                $style .= 'background-image:url("'.esc_url($featured_img_url).'");';
                if($height_in_pixels_0) {
                    $style .= 'height:'.$height_in_pixels_0.'px;';
                }
                if($banner_bgsize) {
                    $style .= 'background-size:'.$banner_bgsize .';';
                }
                if($banner_bgpos) {
                    $style .= 'background-position:'.$banner_bgpos .';';
                }
                if($banner_bgrepeat){
                    $style .= 'background-repeat:'.$banner_bgrepeat .';';
                }
            $style .= '}';
            if($banner_textcolor) {
                $style .= '.banner p { color:' . $banner_textcolor .';}';
            }

            if($banner_color) {
                $style .= '.banner a { color:' . $banner_color .';}';
            }
            $style .= '</style>';
            if($banner_height) {
                $banner = '<div class="banner" style="min-height:' . $banner_height .'px; height:' .$banner_height .'px;">';
            }
            else {
                $banner = '<div class="banner">';
            }
            $banner .= '<div class="innerwrapper">';
            if($banner_image_url) {
                $banner .= '<img src=' . $banner_image_url .' alt="" />';
            }

            if($banner_text) {
                $banner .= '<p>' . nl2br( esc_html( get_post_meta( get_the_ID(), 'meta-textarea', true) ) ); '</p>';
            }
            if($banner_button){
                if($banner_linkpage && $banner_local_link) {
                    $banner .= '<a href="/?page_id=' . $banner_linkpage . $banner_local_link . '" class="button"';
                    $banner .= '">';
                    $banner .= $banner_button;
                    $banner .= '</a>';        
                }
                elseif (!$banner_linkpage && $banner_local_link) {
                        $banner .= '<a href="' . $banner_local_link . '" class="button"';
                        $banner .= '">';
                        $banner .= $banner_button;
                        $banner .= '</a>';       
                    }
                
                else {
                    $banner .= '<div class="button"';
                    $banner .= ' style="color:' . $banner_color ;
                    $banner .= '">';
                    $banner .= $banner_button;
                    $banner .= '</div>';    
    
                }
            }
            $banner .= '</div></div>'; 
            echo $style;
            echo $banner;
        }
    }
}

// Create metaboxes
function rs_custom_meta() {
    $post_types = array ( 'post', 'page' );
    add_meta_box( 
        'rs_meta', 
        __( 'Banner Call to Action', 'rs-call-to-action' ), 
        'rs_meta_callback', 
        $post_types,
        'normal' 
    );
}
add_action( 'add_meta_boxes', 'rs_custom_meta' );

/**
 * Outputs the content of the meta box
 */

function rs_meta_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'rs_nonce' );
    $rs_stored_meta = get_post_meta( $post->ID );
    ?>
     <p>
        <label for="meta-height" class="rs-row-title"><?php _e( 'Banner height in pixels', 'rs-call-to-action' )?></label>
        <input type="text" name="meta-height" id="meta-height" value="<?php if ( isset ( $rs_stored_meta['meta-height'] ) ) echo $rs_stored_meta['meta-height'][0]; ?>" class="meta-height"  />
    </p>
    <p>
        <label for="meta-select-bgsize" class="rs-row-title"><?php _e( 'Background size', 'rs-call-to-action' )?></label>
        <select name="meta-select-bgsize" id="meta-select-bgsize">
            <option value="cover" <?php if ( isset ( $rs_stored_meta['meta-select-bgsize'] ) ) selected( $rs_stored_meta['meta-select-bgsize'][0], 'cover' ); ?>><?php _e( 'cover', 'rs-call-to-action' )?></option>';
            <option value="auto" <?php if ( isset ( $rs_stored_meta['meta-select-bgsize'] ) ) selected( $rs_stored_meta['meta-select-bgsize'][0], 'auto' ); ?>><?php _e( 'auto', 'rs-call-to-action' )?></option>';
            <option value="contain" <?php if ( isset ( $rs_stored_meta['meta-select-bgsize'] ) ) selected( $rs_stored_meta['meta-select-bgsize'][0], 'contain' ); ?>><?php _e( 'contain', 'rs-call-to-action' )?></option>';
        </select>
    </p>

    <p>
        <label for="meta-select-bgpos" class="rs-row-title"><?php _e( 'Background position', 'rs-call-to-action' )?></label>
        <select name="meta-select-bgpos" id="meta-select-bgpos">
            <option value="center top" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'center top' ); ?>><?php _e( 'center top', 'rs-call-to-action' )?></option>';
            <option value="center center" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'center center' ); ?>><?php _e( 'center center', 'rs-call-to-action' )?></option>';
            <option value="center bottom" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'center bottom' ); ?>><?php _e( 'center bottom', 'rs-call-to-action' )?></option>';
            <option value="left top" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'left top' ); ?>><?php _e( 'left top', 'rs-call-to-action' )?></option>';
            <option value="left center" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'left center' ); ?>><?php _e( 'left center', 'rs-call-to-action' )?></option>';    
            <option value="left bottom" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'left bottom' ); ?>><?php _e( 'left bottom', 'rs-call-to-action' )?></option>';
            <option value="right top" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'right top' ); ?>><?php _e( 'right top', 'rs-call-to-action' )?></option>';
            <option value="right center" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'right center' ); ?>><?php _e( 'right center', 'rs-call-to-action' )?></option>';
            <option value="right bottom" <?php if ( isset ( $rs_stored_meta['meta-select-bgpos'] ) ) selected( $rs_stored_meta['meta-select-bgpos'][0], 'right bottom' ); ?>><?php _e( 'right bottom', 'rs-call-to-action' )?></option>';
        </select>
    </p>
    <p>
        <label for="meta-select-bgrepeat" class="rs-row-title"><?php _e( 'Background repeat', 'rs-call-to-action' )?></label>
        <select name="meta-select-bgrepeat" id="meta-select-bgrepeat">
            <option value="repeat" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'repeat' ); ?>><?php _e( 'repeat', 'rs-call-to-action' )?></option>';
            <option value="repeat-x" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'repeat-x' ); ?>><?php _e( 'repeat-x', 'rs-call-to-action' )?></option>';
            <option value="repeat-y" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'repeat-y' ); ?>><?php _e( 'repeat-y', 'rs-call-to-action' )?></option>';
            <option value="no-repeat" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'no-repeat' ); ?>><?php _e( 'no-repeat', 'rs-call-to-action' )?></option>';
            <option value="space" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'space' ); ?>><?php _e( 'space', 'rs-call-to-action' )?></option>';
            <option value="round" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'round' ); ?>><?php _e( 'round', 'rs-call-to-action' )?></option>';
            <option value="initial" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'initial' ); ?>><?php _e( 'initial', 'rs-call-to-action' )?></option>';
            <option value="inherit" <?php if ( isset ( $rs_stored_meta['meta-select-bgrepeat'] ) ) selected( $rs_stored_meta['meta-select-bgrepeat'][0], 'inherit' ); ?>><?php _e( 'inherit', 'rs-call-to-action' )?></option>';

        </select>
    </p>

    <p>
        <label for="meta-textarea" class="rs-row-title"><?php _e( 'Banner text', 'rs-call-to-action' )?></label>
        <textarea name="meta-textarea" id="meta-textarea"><?php if ( isset ( $rs_stored_meta['meta-textarea'] ) ) echo $rs_stored_meta['meta-textarea'][0]; ?></textarea>
    </p>
    <p>
        <label for="meta-text-color" class="rs-row-title"><?php _e( 'Text color', 'rs-call-to-action' )?></label>
        <input type="text" name="meta-text-color" id="meta-text-color" value="<?php if ( isset ( $rs_stored_meta['meta-text-color'] ) ) echo $rs_stored_meta['meta-text-color'][0]; ?>" class="meta-text-color"  />
    </p>
    <p>
        <label for="meta-text" class="rs-row-title"><?php _e( 'Button text', 'rs-call-to-action' )?></label>
        <input type="text" name="meta-text" id="meta-text" value="<?php if ( isset ( $rs_stored_meta['meta-text'] ) ) echo $rs_stored_meta['meta-text'][0]; ?>" />
    </p>
    <p>
        <label for="meta-color" class="rs-row-title"><?php _e( 'Button text Color', 'rs-call-to-action' )?></label>
        <input name="meta-color" type="text" value="<?php if ( isset ( $rs_stored_meta['meta-color'] ) ) echo $rs_stored_meta['meta-color'][0]; ?>" class="meta-color" />
    </p>
    <p>
    <label for="meta-image" class="rs-row-title"><?php _e( 'Image on top of banner', 'rs-call-to-action' )?></label>
        <input type="text" name="meta-image" id="meta-image" value="<?php if ( isset ( $rs_stored_meta['meta-image'] ) ) echo $rs_stored_meta['meta-image'][0]; ?>" />
        <input type="button" id="meta-image-button" class="button" value="<?php _e( 'Choose or Upload an Image', 'rs-call-to-action' )?>" />
    </p>
    <?php
    show_all_posts();
    ?>
    <p>
        <label for="meta-local-link" class="rs-row-title"><?php _e( 'Local link ID', 'rs-call-to-action' )?></label>
        <input type="text" name="meta-local-link" id="meta-local-link" value="<?php if ( isset ( $rs_stored_meta['meta-local-link'] ) ) echo $rs_stored_meta['meta-local-link'][0]; ?>" />
    </p>
    <?php
}

/**
 * Loads the image management javascript
 */
function rs_image_enqueue() {
    global $typenow;
    wp_enqueue_media();

    // Registers and enqueues the required javascript.
    wp_register_script( 'meta-box-image', plugin_dir_url( __FILE__ ) . 'js/meta-box-image.js', array( 'jquery' ) );
    wp_localize_script( 'meta-box-image', 'meta_image',
        array(
            'title' => __( 'Choose or Upload an Image', 'rs-call-to-action' ),
            'button' => __( 'Use this image', 'rs-call-to-action' ),
        )
    );
    wp_enqueue_script( 'meta-box-image' );
}
add_action( 'admin_enqueue_scripts', 'rs_image_enqueue' );


/**
 * Loads the color picker javascript
 */
function rs_color_enqueue() {
    global $typenow;
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'meta-box-color-js', plugin_dir_url( __FILE__ ) . 'js/meta-box-color.js', array( 'wp-color-picker' ) );
}
add_action( 'admin_enqueue_scripts', 'rs_color_enqueue' );

/**
 * Saves the custom meta input
 */
function rs_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'rs_nonce' ] ) && wp_verify_nonce( $_POST[ 'rs_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'meta-height' ] ) ) {
        update_post_meta( $post_id, 'meta-height', $_POST[ 'meta-height' ] );
    }
    if( isset( $_POST[ 'meta-select-bgsize' ] ) ) {
        update_post_meta( $post_id, 'meta-select-bgsize', $_POST[ 'meta-select-bgsize' ] );
    }
    if( isset( $_POST[ 'meta-select-bgpos' ] ) ) {
        update_post_meta( $post_id, 'meta-select-bgpos', $_POST[ 'meta-select-bgpos' ] );
    }
        if( isset( $_POST[ 'meta-select-bgrepeat' ] ) ) {
        update_post_meta( $post_id, 'meta-select-bgrepeat', $_POST[ 'meta-select-bgrepeat' ] );
    }

    if( isset( $_POST[ 'meta-textarea' ] ) ) {
        update_post_meta( $post_id, 'meta-textarea',  $_POST[ 'meta-textarea' ] );
    }
    if( isset( $_POST[ 'meta-text-color' ] ) ) {
        update_post_meta( $post_id, 'meta-text-color', $_POST[ 'meta-text-color' ] );
    }

    if( isset( $_POST[ 'meta-text' ] ) ) {
        update_post_meta( $post_id, 'meta-text', sanitize_text_field( $_POST[ 'meta-text' ] ) );
    }
    if( isset( $_POST[ 'meta-color' ] ) ) {
        update_post_meta( $post_id, 'meta-color', $_POST[ 'meta-color' ] );
    }
    if( isset( $_POST[ 'meta-select' ] ) ) {
        update_post_meta( $post_id, 'meta-select', $_POST[ 'meta-select' ] );
    }
    // Checks for input and saves if needed
    if( isset( $_POST[ 'meta-image' ] ) ) {
        update_post_meta( $post_id, 'meta-image', $_POST[ 'meta-image' ] );
    }
    // Checks for input and saves if needed
    if( isset( $_POST[ 'meta-local-link' ] ) ) {
        update_post_meta( $post_id, 'meta-local-link', $_POST[ 'meta-local-link' ] );
    }
}
add_action( 'save_post', 'rs_meta_save' );


function show_all_posts() {
    

    $args=array(
        'order'=> 'DESC',
        'post_type' => array('post', 'page')
    );

   $query=new WP_Query($args);

    if( $query->have_posts()) { 
        $rs_stored_meta = get_post_meta( get_the_ID() );
        ?>
         <p>
            <label for="meta-select" class="rs-row-title"><?php _e( 'Select page to link to', 'rs-call-to-action' )?></label>
      
        <select name="meta-select" id="meta-select">
        <option value="">Choose page</option>
        <?php
        while( $query->have_posts() ) { 
            $query->the_post();
            $pageid = get_the_ID();
            ?>
            <option value="<?php echo $pageid; ?>"
            <?php if ( isset ( $rs_stored_meta['meta-select'] ) ) selected( $rs_stored_meta['meta-select'][0], $pageid ); ?>>
                    <?php _e( the_title(), 'rs-call-to-action' )?></option>';
            <?php
        }
        ?>
        </select>
        </p>
        <?php
    }
    wp_reset_postdata();
}

/**
 * Generated by the WordPress Option Page generator
 * at http://jeremyhixon.com/wp-tools/option-page/
 */

class RSCallToAction {
	private $rs_call_to_action_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'rs_call_to_action_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'rs_call_to_action_page_init' ) );
	}

	public function rs_call_to_action_add_plugin_page() {
		add_options_page(
			'RS Call To Action', // page_title
			'RS Call To Action', // menu_title
			'manage_options', // capability
			'rs-call-to-action', // menu_slug
			array( $this, 'rs_call_to_action_create_admin_page' ) // function
		);
	}

	public function rs_call_to_action_create_admin_page() {
		$this->rs_call_to_action_options = get_option( 'rs_call_to_action_option_name' ); ?>

		<div class="wrap">
			<h2>RS Call To Action</h2>
			<p>Banner options</p>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'rs_call_to_action_option_group' );
					do_settings_sections( 'rs-call-to-action-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function rs_call_to_action_page_init() {
		register_setting(
			'rs_call_to_action_option_group', // option_group
			'rs_call_to_action_option_name', // option_name
			array( $this, 'rs_call_to_action_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'rs_call_to_action_setting_section', // id
			'Settings', // title
			array( $this, 'rs_call_to_action_section_info' ), // callback
			'rs-call-to-action-admin' // page
		);

		add_settings_field(
			'height_in_pixels_0', // id
			'Height in pixels', // title
			array( $this, 'height_in_pixels_0_callback' ), // callback
			'rs-call-to-action-admin', // page
			'rs_call_to_action_setting_section' // section
		);
	}

	public function rs_call_to_action_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['height_in_pixels_0'] ) ) {
			$sanitary_values['height_in_pixels_0'] = sanitize_text_field( $input['height_in_pixels_0'] );
		}

		return $sanitary_values;
	}

	public function rs_call_to_action_section_info() {
		
	}

	public function height_in_pixels_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="rs_call_to_action_option_name[height_in_pixels_0]" id="height_in_pixels_0" value="%s">',
			isset( $this->rs_call_to_action_options['height_in_pixels_0'] ) ? esc_attr( $this->rs_call_to_action_options['height_in_pixels_0']) : ''
		);
	}

}
if ( is_admin() )
	$rs_call_to_action = new RSCallToAction();

/* 
 * Retrieve this value with:
 * $rs_call_to_action_options = get_option( 'rs_call_to_action_option_name' ); // Array of All Options
 * $height_in_pixels_0 = $rs_call_to_action_options['height_in_pixels_0']; // Height in pixels
 */
